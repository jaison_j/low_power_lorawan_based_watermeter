# LoRaWAN based Watermeter
The main scope of the project is that the module can be retrofitted to any other water meters. The proposed system also works in Low Power which provides a better method for energy consumption.

# Getting Started
- Make sure that you have ULPLoRa board or any LoRaWAN enabled board.
- Install Arduino IDE.
- Install arduino-lmic from here . Copy the library to ~/Arduino/libraries/
- Select board : Arduino Pro Mini 3.3v 8Mhz


# Prerequisites
Arduino IDE [Tested]

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments

# Changelog
