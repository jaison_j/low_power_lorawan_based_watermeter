                                                              //////////////////////////////
                                                             //******* WATERMETER ******///
                                                            //////////////////////////////

#include <Wire.h>
#define loraSerial Serial1
#define debugSerial Serial
///////////////////////////////////
#include <lmic.h>
#include <hal/hal.h>
///////////////////////////////////
#include <EnableInterrupt.h>
///////////////////////////////////
#include <LowPower.h>
#include <avr/wdt.h>
///////////////////////////////////
#include <EEPROM.h>
///////////////////////////////////

float input_volt = 0.0;
float temp=0.0;
float input_volt1 = 0.0;
float temp1=0.0;

static const int MaxCount = 4;
int currentCount = 0;
long sum = 0;

#define ARDUINOPIN 7
#define TRIGGER 8
#define TAMP A5
#define InterruptPin A0

unsigned char a=0,f=0,co=0,tam=0;
volatile uint16_t InterruptCount = 0;
unsigned char x01=0,x1=0; //x1 will be set with the tens digit of the first reading
long int x=0;  //x is water reading in cubic meter,x1 is one point after decimal,x01 is two point after decimal. 
unsigned char lit1=0;

// use low power sleep; comment next line litto not use low power sleep
#define SLEEP

static const PROGMEM u1_t NWKSKEY[16] = { 0xFA, 0xFF, 0xAB, 0xD8, 0x3F, 0x83, 0x5C, 0x70, 0x73, 0x16, 0x1A, 0x1B, 0xEA, 0xFB, 0x99, 0xC8 };
static const u1_t PROGMEM APPSKEY[16] = { 0x50, 0x59, 0x91, 0xDD, 0x1E, 0xF2, 0x2F, 0x2B, 0x15, 0x6F, 0x2D, 0xCF, 0xBC, 0x9A, 0xF3, 0x2C };
static const u4_t DEVADDR = 0x260112AB;

void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

struct data {
  short int Voltage;
  char x01;
  char x1;
  long int x;
  char tamp;
  char error;
}mydata;

static osjob_t sendjob;
const unsigned TX_INTERVAL = 30;
const lmic_pinmap lmic_pins = {
    .nss = 6,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 5,
    .dio = {2,3,4},
};

unsigned char t1=0;
bool next = false;
void onEvent (ev_t ev) {
  switch (ev) {
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      if (LMIC.txrxFlags & TXRX_ACK)
        Serial.println(F("Received ack"));
      if (LMIC.dataLen) {
        Serial.println(F("Received "));
        Serial.println(LMIC.dataLen);
        Serial.println(F(" bytes of payload"));
      }
      next = true;
      break;
      }
}

  void do_send(osjob_t* j)
  { 
    Volt();
    delay(100);
    if(LMIC.opmode & OP_TXRXPEND){
      Serial.println(F("OP_TXRXPEND, not sending"));} 
    else{
      LMIC_setTxData2(1, (unsigned char *)&mydata, sizeof(mydata) - 1, 0);
      Serial.println(F("Packet queued"));} 
      digitalWrite(A2, LOW); 
      InterruptCount = 0;
      delay(100); 
  }

void setup(){
    Serial.begin(9600);
    Serial.println("Initialize WaterMeter");
    pinMode(ARDUINOPIN, INPUT); 
    pinMode(TRIGGER, INPUT);
    pinMode(TAMP, INPUT);
    enableInterrupt(TRIGGER, switch2_interruptFunction, RISING);
    Serial.println(F("Enter setup"));
    pinMode(A1, OUTPUT);//status led
    digitalWrite(A1, HIGH);
    delay(10);
    digitalWrite(A1, LOW);
    pinMode(InterruptPin, INPUT);
    enableInterrupt(InterruptPin, triggerFunction, RISING);
    enableInterrupt(ARDUINOPIN, switch1_interruptFunction, RISING);
//    enableInterrupt(TAMP, tampFunction, RISING);

//lit1=0;
//a=0;
//x01=9;
//x1=8;
//x=86;
//delay(500);
//  EEPROM.put(10, x01);
//  EEPROM.put(50, x1);
//  EEPROM.put(90, x);
//  delay(500);
  EEPROM.get(10, x01);
  EEPROM.get(50, x1);
  EEPROM.get(90, x);
  a=x01;
  lit1=a;
  delay(500);

    
    // LMIC init
    
    os_init();
    LMIC_reset();
    #ifdef PROGMEM
        uint8_t appskey[sizeof(APPSKEY)];
        uint8_t nwkskey[sizeof(NWKSKEY)];
        memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
        memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
        LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
        LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif
    #if defined(CFG_eu868)
        LMIC_setupChannel(0, 865062500, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
        LMIC_setupChannel(1, 865402500, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
        LMIC_setupChannel(2, 865985000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    #elif defined(CFG_us915)
        LMIC_selectSubBand(1);
    #endif
    LMIC_setLinkCheckMode(0);
    LMIC.dn2Dr = DR_SF9;
    LMIC_setDrTxpow(DR_SF7, 14);
    // Start job
    do_send(&sendjob);
    // Wait a maximum of 10s for Serial Monitor
    while (!debugSerial && millis() < 10000);
    delay(500);f=0;delay(10);

    do_send(&sendjob);
    delay(100);tam=0;
    for(int i=0;i<50;i++)
    {
       delay(50);
       os_runloop_once();
    }
}

void loop() 
{
   x01=a;
   EEPROM.put(10, x01);
   t1=a;
  if(x01>9)
  {
      x1++;
      EEPROM.put(50, x1);
      x01=0;a=0;
  }
  if(x1>9)
  {
      x++;  
  EEPROM.put(90, x);
      x1=0;
  } 

  Serial.print("x = ");Serial.println(x);
  Serial.print("x1 = ");Serial.println(x1);
  Serial.print("x01 = ");Serial.println(x01);

  extern volatile unsigned long timer0_overflow_count;
    
  if (InterruptCount==1) {
    cli();
    co=1;
    do_send(&sendjob);
    delay(100);tam=0;
    for(int i=0;i<50;i++)
    {
       delay(50);
       os_runloop_once();
    }InterruptCount=0;
    Serial.println("LoRa send..................");
    sei();
  } 
//  else if(tam>0){
//      for(int j=0;j<=10;j++)
//      {
//          do_send(&sendjob);
//          delay(100);
//          for(int i=0;i<50;i++)
//          {
//             delay(50);
//             os_runloop_once();
//          }  InterruptCount=0;
//          Serial.println("LoRa send..................");
//      }
//    }
  else {
    Serial.print(F("Enter sleeping forever "));
    Serial.flush(); // give the serial print chance to complete
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
    cli();
    timer0_overflow_count += 8 * 64 * clockCyclesPerMicrosecond();
    sei();
    Serial.println(F("Sleep complete"));
    next = false;
  }  
}

void switch2_interruptFunction() 
{
    if(f==2)
  {
    a++;
    co=0;
    f=1; 
  }f=1;
     disableInterrupt(TRIGGER); 
     enableInterrupt(ARDUINOPIN, switch1_interruptFunction, RISING);
}

void switch1_interruptFunction()
{
    if(f==1)
      {
        f=2;
      }

    disableInterrupt(ARDUINOPIN);
    enableInterrupt(TRIGGER, switch2_interruptFunction, RISING);
}

//void tampFunction() 
//{
//    tam=1;
//    delay(100);
//    Serial.print("tam in fun=");
//    Serial.println(tam);
//    enableInterrupt(TAMP, tampFunction, RISING);
//    #ifdef DEBUG
//        Serial.flush();
//    #endif
//}

void triggerFunction() {
  InterruptCount ++;
  digitalWrite(A2, HIGH);
  Serial.print("InterruptCount=");
  Serial.println(InterruptCount);
}

void Volt()
{
    float analogvalue;
    for (byte  i = 0; i < 10; i++){
    analogvalue += analogRead(A3);
//    Serial.print("analogvalue= ");               
//    Serial.println(analogRead(A3));
    }
    analogvalue=analogvalue/10;
    temp = ((analogvalue* 3.3)/1024);  //ADC voltage*Ref. Voltage/1024
    float temp1=temp*2;
    float sum=0;
    double avg=0;
    for (byte  i = 0; i < 4; i++){
    sum += temp1;}
    avg = sum / 4;
    int tp=avg*100;
    Serial.print("Voltage= ");               
    Serial.print(avg);
    Serial.println("V");
    mydata.Voltage=tp;
    mydata.x01=x01; mydata.x1=x1; mydata.x=x;
    mydata.tamp=tam;
    tp=0;}
